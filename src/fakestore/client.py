from keboola.http_client import HttpClient
from typing import List, Dict

BASE_URL = "https://fakestoreapi.com/"
ENDPOINT_PRODUCTS = "products"


class FakeStoreClient(HttpClient):
    def __init__(self):
        super().__init__(BASE_URL)

    def get_products(self) -> List[Dict]:
        products = self.get(endpoint_path=ENDPOINT_PRODUCTS)
        return products
